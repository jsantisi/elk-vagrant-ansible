# Elasticsearch and Kibana with Vagrant #
This project is for developers looking to explore Elasticsearch and Kibana.  I used this project to configure a single node Elastictsearch instance with Vagrant and Ansible.

Why Vagrant? Well - vagrant is really straight forward for development and testing your playbooks/cookbooks.  Also, plugs and plays with your favorite IaC framework such as Chef, Puppet and Ansible.

My development team uses Chef; however, I chose to get first hand exposure to Ansible.  Let me tell you that I am a big fan.  The main difference with Chef is that you don't have to be a Ruby guru.  Ansible uses playbooks (similar to cookbooks) in yaml format.  So it's very straight forward to define and execute playbooks.

## Project Contents ##
* Vagrantfile: this is how you define the virtualmachine.
* provision/playbook.yml: the ansible playbook that we invoke from the Vagrantfile to provision our VM.
* provision/include/mapping.json: the mapping file for the Elasticsearch Index.

## Quick Start ##
1. Clone this repo
2. Navigate to the directory
3. Run the following from the command line:
```
:> vagrant up
```
4. Open a browser and go to <http://localhost:5601/app/kibana#/home?_g=()>

You should see Kibana up and running.

## Next Step ##
Ingest data!
